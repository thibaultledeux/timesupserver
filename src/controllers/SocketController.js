const TimesupUpdater = require('../metier/presentation/TimesupUpdater')

class SocketController
{
    constructor(roomId, data)
    {
        this._data           = data
        this._roomId         = roomId
        this._timesupUpdater = new TimesupUpdater()
    }

    /**
     *
     * @return {Promise<boolean|Room[]>}
     */
    async getRooms()
    {
        return await this._timesupUpdater.getRooms()
    }

    /**
     *
     * @return {Promise<Room>}
     */
    async getRoom()
    {
        return await this._timesupUpdater.getRoom(this._roomId)
    }

    /**
     * set les rooms
     *
     * @return {Promise<boolean>}
     */
    async setRooms()
    {
        for (let i = 0; i < this._data.length; i++)
        {
            let room = this._data[i]
            await this._timesupUpdater.setRoom(room)
        }

        return true
    }

    /**
     * créer les mots pour une room
     *
     * @return {Promise<boolean>}
     */
    async createWords()
    {
        await this._timesupUpdater.createWordsForRoom(this._roomId, this._data)

        return this._timesupUpdater.getRoom(this._roomId)
    }

    /**
     * set un joeur ready
     *
     * @return {Promise<Room>}
     */
    async setPlayerReady()
    {
        let room = await this._timesupUpdater.getRoom(this._roomId)

        const player = this.getFirstElt()
        room.setPlayerReady(player)
        await this._timesupUpdater.setRoom(room)

        return room
    }

    /**
     * set une step done
     *
     * @return {Promise<Room>}
     */
    async saveTurn()
    {
        const room     = this.getFirstElt()
        const stepDone = this.getSecondElt()

        // set la step faite
        room.steps.forEach((step) => {
            if (step.id === stepDone.id)
            {
                step.done = true
            }
        })

        // remet a zero les mots pour le prochain tours
        room.restartWords()

        // mélange les mots
        room.shuffleWords()

        // sauvegarde la room
        await this._timesupUpdater.setRoom(room)

        return room
    }

    /**
     * set le joueur qui joue et mets tous les autres joueurs en mode not playing
     *
     * @return {Promise<Room>}
     */
    async setPlayerPlaying()
    {
        // recupère la room
        let room = await this._timesupUpdater.getRoom(this._roomId)

        // incremente l'ordre de jeu des team
        room.teamPlayingOrder++
        if (room.teamPlayingOrder === room.teams.length)
        {
            room.teamPlayingOrder = 0
        }

        // recupère la team correspondant a l'ordre
        let teamPlaying = room.getTeamByOrder(room.teamPlayingOrder)

        // set le joueur qui joue
        room.setPlayerPlaying(teamPlaying)

        // set la room
        await this._timesupUpdater.setRoom(room)

        // renvoie la room
        return room
    }

    /**
     * restart la game
     *
     * @return {Promise<Room>}
     */
    async restartRoom()
    {
        // recupère la room
        let room = await this._timesupUpdater.getRoom(this._roomId)

        // restart la room
        room.restart()

        // set la room
        await this._timesupUpdater.setRoom(room)

        // renvoie la room
        return room
    }

    /**
     * supprime la game
     *
     * @return {Promise<boolean>}
     */
    async deleteRoom()
    {
        // supprime la room
        await this._timesupUpdater.deleteRoom(this._roomId)

        // renvoie la confimation
        return true
    }

    /**
     * valide les mots
     *
     * @return {Promise<Word[]>}
     */
    async validateWords()
    {
        const room = await this._timesupUpdater.getRoom(this._roomId)

        return room.wordsExist(this._data)
    }

    /**
     * recupère le 1er element
     *
     * @return {Object}
     */
    getFirstElt()
    {
        const elt = typeof this._data[0] !== 'undefined' ? this._data[0] : null
        if (null === elt)
        {
            throw new Error(`${__filename} : pas d'element a setter`)
        }

        return elt
    }

    /**
     * recupère le 1er element
     *
     * @return {Object}
     */
    getSecondElt()
    {
        const elt = typeof this._data[1] !== 'undefined' ? this._data[1] : null
        if (null === elt)
        {
            throw new Error(`${__filename} : pas d'element a setter`)
        }

        return elt
    }
}

module.exports = SocketController;
