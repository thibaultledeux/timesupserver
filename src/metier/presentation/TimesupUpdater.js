const RoomFileAdapter   = require('../adapters/file/RoomFileAdapter');
const WordFileAdapter   = require('../adapters/file/WordFileAdapter');
const PlayerFileAdapter = require('../adapters/file/PlayerFileAdapter');
const TeamFileAdapter   = require('../adapters/file/TeamFileAdapter');
const StepFileAdapter   = require('../adapters/file/StepFileAdapter');
const FolderFileTools   = require('../../utils/FolderFileTools');
const fs                = require('fs');
const path              = require('path');

class TimesupUpdater
{
    constructor()
    {
        this._pathDirRoom   = `${__dirname}/../../../data/rooms`
        this._roomAdapter   = new RoomFileAdapter();
        this._wordAdapter   = new WordFileAdapter();
    }

    /**
     * recupère toutes les rooms
     *
     * @returns {Promise<Room[]>}
     */
    async getRooms()
    {
        if (false === FolderFileTools.isDir(this._pathDirRoom))
        {
            throw new Error(`${__filename} : pas de dossier des rooms`)
        }


        const roomDirs = fs.readdirSync(this._pathDirRoom).map(dirName => {
            return path.join(this._pathDirRoom, dirName)
        }).filter(FolderFileTools.isDir)

        let rooms = []
        for (let i = 0; i < roomDirs.length; i++)
        {
            let roomId = parseInt(path.basename(roomDirs[i]))
            if (roomId === 0)
            {
                throw new Error(`${__filename} : Impossible de recupèrer lid du room avc le dossier ${roomDirs[i]}`)
            }

            this._roomAdapter.setPathDir(roomId)
            let room = await this._roomAdapter.get(roomId)
            rooms.push(room)
        }

        return rooms
    }

    /**
     * recupère une room
     *
     * @param {number}  roomId  id de la room
     * @returns {Promise<Room>}
     */
    async getRoom(roomId)
    {
        if (roomId === 0)
        {
            throw new Error(`${__filename} : pas d'id de room`)
        }

        if (false === FolderFileTools.isDir(path.join(this._pathDirRoom, `${roomId}`)))
        {
            throw new Error(`${__filename} : pas de dossier de la room ${roomId}`)
        }

        this._roomAdapter.setPathDir(roomId)
        return await this._roomAdapter.get(roomId)
    }

    /**
     * creer une room
     *
     * @param {Room}  room  une room
     *
     * @returns {Promise<boolean>}
     */
    async setRoom(room)
    {
        this._roomAdapter.setPathDir(room.id)
        return await this._roomAdapter.set(room)
    }

    /**
     * creer des most pour une room
     *
     * @param {number}  roomId  id dune room
     * @param {Word[]}  words   les mots a creer
     *
     * @returns {Promise<boolean>}
     */
    async createWordsForRoom(roomId, words)
    {
        for (let i = 0; i < words.length; i++)
        {
            let word = words[i]
            await this.setWord(roomId, word)
        }

        return true
    }

    /**
     * valide un mot de la room
     *
     * @param {number}  roomId  id dune room
     * @param {Word}    word    un mot
     *
     * @returns {Promise<boolean>}
     */
    async setWord(roomId, word)
    {
        this._wordAdapter.setPathDir(roomId)
        return await this._wordAdapter.set(word)
    }

    /**
     * supprime une room
     *
     * @param {number}  roomId  id de la room
     *
     * @return {Promise<boolean>}
     */
    async deleteRoom(roomId)
    {
        this._roomAdapter.setPathDir(roomId)
        return await this._roomAdapter.delete(roomId)
    }


}

module.exports = TimesupUpdater