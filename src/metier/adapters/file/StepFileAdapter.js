const Step            = require('../../models/Step')
const FileAdapter     = require('./FileAdapter')
const FolderFileTools = require('../../../utils/FolderFileTools')
const fs              = require('fs')
const path            = require('path')

const csv             = require('csv-parser')

const COL_ID    = 'id'
const COL_NAME  = 'name'
const COL_DONE  = 'done'
const COL_ORDER = 'order'

class StepFileAdapter extends FileAdapter
{
    /**
     * set path dir
     *
     * @param {number}  roomId  id de la room
     */
    setPathDir(roomId)
    {
        this._pathDir = path.join(this.folderData, `rooms`, `${roomId}`, `steps`)
        this.createPathDirFolder()
    }

    /**
     *
     * recupère une step
     *
     * @param {number}  stepId  id de letape
     *
     * @returns {Promise<Step>}
     */
    async get(stepId)
    {
        if (this._pathDir === null)
        {
            throw new Error(`${__filename} : variable _pathDir non definie`)
        }

        const pathFile = path.join(this._pathDir, `${stepId}.csv`)
        if (false === FolderFileTools.existFile(pathFile))
        {
            throw new Error(`${__filename} : le fichier nexiste pas ${pathFile}`)
        }

        return await this.convertCsvToStep(stepId, pathFile)
    }

    /**
     * set une step
     *
     * @param {Step}  step  une step
     *
     * @returns {Promise<boolean>}
     */
    async set(step)
    {
        if (this._pathDir === null)
        {
            throw new Error(`${__filename} : variable _pathDir non definie`)
        }

        return await this.convertStepToCsv(step)
    }

    /**
     * convertie un fichier csv d'une step en objet step
     *
     * @param {number}  stepId    id de la step
     * @param {string}  pathFile  chemin du ficher de step
     *
     * @returns {Promise<Step>}
     */
    convertCsvToStep(stepId, pathFile)
    {
        return new Promise(((resolve, reject) => {
            try
            {
                let step = null
                fs.createReadStream(pathFile)
                  .pipe(csv())
                  .on('data', (row) =>
                  {
                      step = new Step(
                          parseInt(row[COL_ID]),
                          row[COL_NAME],
                          JSON.parse(row[COL_DONE]),
                          parseInt(row[COL_ORDER])
                      )
                  })
                  .on('end', async () =>
                  {
                      resolve(step)
                  })
            }
            catch (e)
            {
                reject(e)
            }
        }))
    }

    /**
     * convertie un objet setp en fichier csv
     *
     * @param {Step}  step  une step
     *
     * @returns {Promise<boolean>}
     */
    async convertStepToCsv(step)
    {
        return await this.convertObjectToCsv(
            path.join(this._pathDir, `${step.id}.csv`),
            [
                {id: COL_ID, title: COL_ID},
                {id: COL_NAME, title: COL_NAME},
                {id: COL_DONE, title: COL_DONE},
                {id: COL_ORDER, title: COL_ORDER},
            ],
            [{
                [COL_ID]:   step.id,
                [COL_NAME]: step.name,
                [COL_DONE]: step.done,
                [COL_ORDER]: step.order,
            }]
        )
    }
}


module.exports = StepFileAdapter