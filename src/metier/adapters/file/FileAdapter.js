const FolderFileTools = require('../../../utils/FolderFileTools');
const fs              = require('fs');
const path            = require('path');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

class FileAdapter
{
    /**
     * constructor
     */
    constructor()
    {
        this._pathDir    = null
        this._folderData = `${__dirname}/../../../../data`
    }

    /**
     * creer le dossier de pathdir
     *
     * @return {boolean}
     */
    createPathDirFolder()
    {
        if (this._pathDir === null)
        {
            throw new Error(`${__filename} : pathDir non defini`)
        }

        return FolderFileTools.createFolder(this._pathDir)
    }

    /**
     * supprime tous les fichiers contenus dans le dossier de pathDir
     */
    cleanPathDirFolder()
    {
        const files = fs.readdirSync(this._pathDir).map(fileName => {
            return path.join(this._pathDir, fileName)
        }).filter(FolderFileTools.isFile)

        files.forEach((file) => {
            fs.unlinkSync(file)
        })
    }

    /**
     * recupère les enfants
     *
     * @param {string}            folder                      dossiers des enfants
     * @param {FileAdapter}       adapter                     paramètre de lexception
     * @param {array}             setPathDirParamsBase        tableau des paramètre pour setPathDir
     * @param {boolean}           addChildToSetPathDirParams  determine si on ajoute l'id de lenfant au params de setPathDir de l'apadter
     * @param {array}             adapterGetterParamsBase     parametre pour get du adapter
     * @param {boolean}           isDirChildren               st des enfants dossiers ou pas
     *
     * @return {Promise<[]>}
     */
    async getChildren(
        folder,
        adapter,
        setPathDirParamsBase,
        addChildToSetPathDirParams,
        adapterGetterParamsBase,
        isDirChildren = false
    )
    {

        const childrenDir = path.join(this._pathDir, folder)
        if (false === FolderFileTools.isDir(childrenDir))
        {
            throw new Error(`${__filename} : pas de dossier enfants ${childrenDir}`)
        }

        let childrenElt = null
        if (isDirChildren === true)
        {
            childrenElt = fs.readdirSync(childrenDir).map(fileName => {
                return path.join(childrenDir, fileName)
            }).filter(FolderFileTools.isDir)
        }
        else
        {
            childrenElt = fs.readdirSync(childrenDir).map(fileName => {
                return path.join(childrenDir, fileName)
            }).filter(FolderFileTools.isFile)
        }

        let children = []
        for (let i = 0; i < childrenElt.length; i++)
        {
            let childElt        = childrenElt[i]
            let childEltSplited = childElt.split(`.`)
            let childId         = parseInt(path.basename(childEltSplited[0]));
            if (childId === 0 || Number.isNaN(childId))
            {
                throw new Error(`${__filename} : Impossible de recupèrer l'id de l'enfant avc le fichier ${childElt}`)
            }

            let setPathDirParams = setPathDirParamsBase
            if (addChildToSetPathDirParams === true)
            {
                setPathDirParams = [...setPathDirParams, childId]
            }
            adapter.setPathDir(...setPathDirParams)

            let adapterGetterParams = [...adapterGetterParamsBase, childId]
            let child = await adapter.get(...adapterGetterParams)
            children.push(child)
        }

        return children
    }

    /**
     * convertie un objet en csv
     *
     * @param {string}  pathFile  chemin du fichier csv
     * @param {array}   header    header du csv
     * @param {array}   data      données du csv
     *
     * @return {Promise<boolean>}
     */
    async convertObjectToCsv(pathFile, header, data)
    {
        if (FolderFileTools.existFile(pathFile))
        {
            fs.unlinkSync(pathFile)
        }

        const csvWriter = createCsvWriter({
            path:   pathFile,
            header: header
        })

        await csvWriter.writeRecords(data)
        return true
    }

    /**
     * supprime l'objet
     *
     * @return {Promise<boolean>}
     */
    async deleteObject()
    {
        FolderFileTools.deleteFolderAndChildren(this._pathDir)

        return  true
    }

    /**
     * a surcharger
     */
    setPathDir(...params) {}

    /**
     * a surcharger
     */
    async get(...params)  {}

    /**
     * a surcharger
     */
    async set(...params)  {}



    get folderData ()
    {
        return this._folderData
    }
}


module.exports = FileAdapter;