const Player          = require('../../models/Player')
const FileAdapter     = require('./FileAdapter')
const FolderFileTools = require('../../../utils/FolderFileTools')
const fs              = require('fs')
const path            = require('path')
const csv             = require('csv-parser')

const COL_ID          = 'id'
const COL_NAME        = 'name'
const COL_TEAMID      = 'teamId'
const COL_READY       = 'ready'
const COL_PLAYING     = 'playing'
const COL_ORDER       = 'order'
const COL_ISLASTREADY = 'isLastReady'

class PlayerFileAdapter extends FileAdapter
{
    /**
     * set path dir
     *
     * @param {number}  roomId  id de la room
     * @param {number}  teamId  id de la team
     */
    setPathDir(roomId, teamId)
    {
        this._pathDir = path.join(this.folderData, `rooms`, `${roomId}`, 'teams', `${teamId}`, `players`)
        this.createPathDirFolder()
    }

    /**
     * recupère un player
     *
     * @param {number}  playerId  id du player
     *
     * @returns {Promise<Player>}
     */
    async get(playerId)
    {
        if (this._pathDir === null)
        {
            throw new Error(`${__filename} : variable _pathDir non definie`)
        }

        const pathFile = path.join(this._pathDir, `${playerId}.csv`)
        if (false === FolderFileTools.existFile(pathFile))
        {
            throw new Error(`${__filename} : le fichier nexiste pas ${pathFile}`)
        }

        return await this.convertCsvToPlayer(playerId, pathFile)

    }

    /**
     * set un player
     *
     * @param {Player}  player  un player
     *
     * @returns {Promise<boolean>}
     */
    async set(player)
    {
        if (this._pathDir === null)
        {
            throw new Error(`${__filename} : variable _pathDir non definie`)
        }

        return await this.convertPlayerToCsv(player)
    }

    /**
     * convertie un fichier csv d'un player en objet player
     *
     * @param {number}  playerId  id du joueur
     * @param {string}  pathFile  chemin du ficher de player
     *
     * @returns {Promise<Player>}
     */
    async convertCsvToPlayer(playerId, pathFile)
    {
        return new Promise((resolve, reject) => {
            try
            {
                let player = null
                fs.createReadStream(pathFile)
                  .pipe(csv())
                  .on('data', (row) =>
                  {
                      player = new Player(
                          parseInt(row[COL_ID]),
                          row[COL_NAME],
                          JSON.parse(row[COL_READY]),
                          JSON.parse(row[COL_PLAYING]),
                          parseInt(row[COL_ORDER]),
                          JSON.parse(row[COL_ISLASTREADY])
                      )
                  })
                  .on('end', async () =>
                  {
                      resolve(player)
                  })
            }
            catch (e)
            {
                reject(e)
            }

        })
    }

    /**
     * convertie un objet player en fichier csv
     *
     * @param {Player}  player  un player
     *
     * @returns {Promise<boolean>}
     */
    async convertPlayerToCsv(player)
    {
        return await this.convertObjectToCsv(
            path.join(this._pathDir, `${player.id}.csv`),
            [
                {id: COL_ID, title: COL_ID},
                {id: COL_NAME, title: COL_NAME},
                {id: COL_READY, title: COL_READY},
                {id: COL_PLAYING, title: COL_PLAYING},
                {id: COL_ORDER, title: COL_ORDER},
                {id: COL_ISLASTREADY, title: COL_ISLASTREADY},
            ],
            [{
                [COL_ID]:   player.id,
                [COL_NAME]: player.name,
                [COL_READY]: player.ready,
                [COL_PLAYING]: player.playing,
                [COL_ORDER]: player.order,
                [COL_ISLASTREADY]: player.isLastReady,
            }]
        )
    }
}


module.exports = PlayerFileAdapter