const Word            = require('../../models/Word')
const FileAdapter     = require('./FileAdapter')
const FolderFileTools = require('../../../utils/FolderFileTools')
const fs              = require('fs')
const path            = require('path')
const csv             = require('csv-parser')

const COL_ID   = 'id'
const COL_TEXT = 'name'
const COL_DONE = 'done'

class WordFileAdapter extends FileAdapter
{
    /**
     * set path dir
     *
     * @param {number}  roomId  id de la room
     */
    setPathDir(roomId)
    {
        this._pathDir = path.join(this.folderData, `rooms`, `${roomId}`, `words`)
        this.createPathDirFolder()
    }

    /**
     *
     * recupère un mot
     *
     * @param {number}  wordId  id d'un mot
     *
     * @returns {Promise<Word>}
     */
    async get(wordId)
    {
        if (this._pathDir === null)
        {
            throw new Error(`${__filename} : variable _pathDir non definie`)
        }

        const pathFile = path.join(this._pathDir, `${wordId}.csv`)
        if (false === FolderFileTools.existFile(pathFile))
        {
            throw new Error(`${__filename} : le fichier nexiste pas ${pathFile}`)
        }

        return await this.convertCsvToWord(wordId, pathFile)
    }

    /**
     * set un mot
     *
     * @param {Word}  word  un mot
     *
     * @returns {Promise<boolean>}
     */
    async set(word)
    {
        if (this._pathDir === null)
        {
            throw new Error(`${__filename} : variable _pathDir non definie`)
        }

        return await this.convertWordToCsv(word)
    }

    /**
     * convertie un fichier csv d'un word en objet word
     *
     * @param {number}  wordId  id du mot
     * @param {string}  pathFile    chemin du ficher de word
     *
     * @returns {Promise<Word>}
     */
    convertCsvToWord(wordId, pathFile)
    {
        return new Promise((resolve, reject) => {
            try
            {
                let word = null
                fs.createReadStream(pathFile)
                  .pipe(csv())
                  .on('data', (row) =>
                  {
                      word = new Word(
                          parseInt(row[COL_ID]),
                          row[COL_TEXT],
                          row[COL_DONE]
                      )
                  })
                  .on('end', async () =>
                  {
                      resolve(word)
                  })
            }
            catch (e)
            {
                reject(e)
            }
        })
    }

    /**
     * convertie un objet word en fichier csv
     *
     * @param {Word}  word  un word
     *
     * @returns {Promise<boolean>}
     */
    async convertWordToCsv(word)
    {
        return await this.convertObjectToCsv(
            path.join(this._pathDir, `${word.id}.csv`),
            [
                {id: COL_ID, title: COL_ID},
                {id: COL_TEXT, title: COL_TEXT},
                {id: COL_DONE, title: COL_DONE},
            ],
            [{
                [COL_ID]:   word.id,
                [COL_TEXT]: word.text,
                [COL_DONE]: word.done,
            }]
        )
    }
}


module.exports = WordFileAdapter