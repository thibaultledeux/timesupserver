const Team              = require('../../models/Team');
const FileAdapter       = require('./FileAdapter');
const PlayerFileAdapter = require('./PlayerFileAdapter');
const FolderFileTools   = require('../../../utils/FolderFileTools');
const fs                = require('fs');
const path              = require('path');

const csv             = require('csv-parser');

const COL_ID                = 'id';
const COL_SCORE             = 'score';
const COL_ORDER             = 'order';
const COL_PLAYINPLAYERORDER = 'playerPlayingOrder';

class TeamFileAdapter extends FileAdapter
{
    constructor()
    {
        super()
        this._playerFileAdapter = new PlayerFileAdapter()
    }

    /**
     * set le pathdir
     *
     * @param {number}  roomId  id de la room
     * @param {number}  teamId  id de la team
     */
    setPathDir(roomId, teamId)
    {
        this._pathDir = path.join(this.folderData, 'rooms', `${roomId}`, 'teams')
        this.createPathDirFolder()

        this._pathDir = path.join(this.folderData, 'rooms', `${roomId}`, 'teams', `${teamId}`)
        this.createPathDirFolder()
    }

    /**
     * recupère un team
     *
     * @param {number}  roomId  id de la room
     * @param {number}  teamId  id de la team
     *
     * @returns {Promise<Team>}
     */
    async get(roomId, teamId)
    {
        if (this._pathDir === null)
        {
            throw new Error(`${__filename} : variable _pathDir non definie`)
        }

        const pathFile = path.join(this._pathDir, `${teamId}.csv`)
        if (false === FolderFileTools.existFile(pathFile))
        {
            throw new Error(`${__filename} : le fichier n'existe pas ${pathFile}`)
        }

        return await this.convertCsvToTeam(roomId, teamId, pathFile);
    }

    /**
     * set un team
     *
     * @param {number} roomId  id de l room
     * @param {Team}   team    un team
     *
     * @returns {Promise<boolean>}
     */
    async set(roomId, team)
    {
        if (this._pathDir === null)
        {
            throw new Error(`${__filename} : variable _pathDir non definie`)
        }

        this._playerFileAdapter.setPathDir(roomId, team.id)
        for (let i = 0; i < team.players.length; i++)
        {
            let player = team.players[i]
            await this._playerFileAdapter.set(player)
        }

        return await this.convertTeamToCsv(team)
    }

    /**
     * convertie un fichier csv d'un team en objet team
     *
     * @param {number}  roomId  id de la room
     * @param {number}  teamId  id de la team
     * @param {string}  pathFile    chemin du ficher de team
     *
     * @returns {Promise<Team>}
     */
    async convertCsvToTeam(roomId, teamId, pathFile)
    {
        return new Promise(async (resolve, reject) => {
            try
            {
                let team      = null
                const players = await this.getChildren(
                    'players',
                    this._playerFileAdapter,
                    [roomId, teamId],
                    false,
                    []
                )

                fs.createReadStream(pathFile)
                  .pipe(csv())
                  .on('data', (row) =>
                  {
                      team = new Team(
                          parseInt(row[COL_ID]),
                          players,
                          parseInt(row[COL_SCORE]),
                          parseInt(row[COL_ORDER]),
                          parseInt(row[COL_PLAYINPLAYERORDER])
                      )
                  })
                  .on('end', async () =>
                  {
                      resolve(team)
                  })
            }
            catch (e)
            {
                reject(e)
            }
        })
    }

    /**
     * convertie un objet team en fichier csv
     *
     * @param {Team}    team    un team
     *
     * @returns {Promise<boolean>}
     */
    async convertTeamToCsv(team)
    {
        return await this.convertObjectToCsv(
            path.join(this._pathDir, `${team.id}.csv`),
            [
                {id: COL_ID, title: COL_ID},
                {id: COL_SCORE, title: COL_SCORE},
                {id: COL_ORDER, title: COL_ORDER},
                {id: COL_PLAYINPLAYERORDER, title: COL_PLAYINPLAYERORDER},
            ],
            [{
                [COL_ID]:    team.id,
                [COL_SCORE]: team.score,
                [COL_ORDER]: team.order,
                [COL_PLAYINPLAYERORDER]: team.playerPlayingOrder,
            }]
        )
    }
}


module.exports = TeamFileAdapter;