const fs              = require('fs')
const path            = require('path')
const csv             = require('csv-parser')
const FileAdapter     = require('./FileAdapter')
const Room            = require('../../models/Room')
const TeamFileAdapter = require('./TeamFileAdapter')
const StepFileAdapter = require('./StepFileAdapter')
const WordFileAdapter = require('./WordFileAdapter')
const FolderFileTools = require('../../../utils/FolderFileTools')

const COL_ID               = 'id';
const COL_NAME             = 'name';
const COL_CHRONO           = 'chrono';
const COL_LIMITNBWORDS     = 'limitNbWords';
const COL_TEAMPLAYINGORDER = 'teamPlayingOrder';

class RoomFileAdapter extends FileAdapter
{
    constructor()
    {
        super()
        this._teamFileAdapter = new TeamFileAdapter()
        this._stepFileAdapter = new StepFileAdapter()
        this._wordFileAdapter = new WordFileAdapter()
    }

    /**
     * set le pathdir pour une room
     *
     * @param {number}  roomId  id de la room
     */
    setPathDir(roomId)
    {
        this._pathDir = path.join(this.folderData, 'rooms')
        this.createPathDirFolder()

        this._pathDir = path.join(this.folderData, 'rooms', `${roomId}`)
        this.createPathDirFolder()
    }

    /**
     * recupère un room
     *
     * @returns {Promise<Room>}
     */
    async get(roomId)
    {
        if (this._pathDir === null)
        {
            throw new Error(`${__filename} : variable _pathDir non definie`)
        }

        const pathFile = path.join(this._pathDir, `${roomId}.csv`)
        if (false === FolderFileTools.existFile(pathFile))
        {
            throw new Error(`${__filename} : le fichier n'existe pas ${pathFile}`)
        }

        return await this.convertCsvToRoom(roomId, pathFile)
    }

    /**
     * set un room
     *
     * @param {Room}  room  un room
     *
     * @returns {Promise<boolean>}
     */
    async set(room)
    {
        if (this._pathDir === null)
        {
            throw new Error(`${__filename} : variable _pathDir non definie`)
        }

        this._stepFileAdapter.setPathDir(room.id)
        for (let i = 0; i < room.steps.length; i++)
        {
            let step = room.steps[i]
            await this._stepFileAdapter.set(step)
        }

        this._wordFileAdapter.setPathDir(room.id)
        await this._wordFileAdapter.deleteObject()
        this._wordFileAdapter.setPathDir(room.id)
        for (let i = 0; i < room.words.length; i++)
        {
            let word = room.words[i]
            await this._wordFileAdapter.set(word)
        }

        for (let i = 0; i < room.teams.length; i++)
        {
            let team = room.teams[i]

            this._teamFileAdapter.setPathDir(room.id, team.id)
            await this._teamFileAdapter.set(room.id, team)
        }

        await this.convertRoomToCsv(room)
    }

    /**
     * supprime la room
     *
     * @param {number}  roomId  id de la room
     *
     * @return {Promise<boolean>}
     */
    async delete(roomId)
    {
        if (this._pathDir === null)
        {
            throw new Error(`${__filename} : variable _pathDir non definie`)
        }

        return await this.deleteObject()
    }

    /**
     * convertie un fichier csv d'un room en objet room
     *
     * @param {number}  roomId        id de la room
     * @param {string}  pathFile  chemin du ficher de room
     *
     * @returns {Promise<Room>}
     */
    async convertCsvToRoom(roomId, pathFile)
    {
        return new Promise(async (resolve, reject) => {
            try
            {
                let room    = null
                const teams = await this.getChildren(
                    'teams',
                    this._teamFileAdapter,
                    [roomId],
                    true,
                    [roomId],
                    true
                )

                const steps = await this.getChildren(
                    'steps',
                    this._stepFileAdapter,
                    [roomId],
                    false,
                    []
                )

                const words = await this.getChildren(
                    'words',
                    this._wordFileAdapter,
                    [roomId],
                    false,
                    []
                )

                fs.createReadStream(pathFile)
                  .pipe(csv())
                  .on('data', (row) =>
                  {
                      room = new Room(
                          parseInt(row[COL_ID]),
                          row[COL_NAME],
                          parseInt(row[COL_CHRONO]),
                          parseInt(row[COL_LIMITNBWORDS]),
                          parseInt(row[COL_TEAMPLAYINGORDER]),
                          teams,
                          words,
                          steps
                      )
                  })
                  .on('end', async () =>
                  {
                      resolve(room)
                  })
            }
            catch (e)
            {
                reject(e)
            }
        })

    }

    /**
     * convertie un objet room en fichier csv
     *
     * @param {Room}  room  un room
     *
     * @returns {Promise<boolean>}
     */
    async convertRoomToCsv(room)
    {
        return await this.convertObjectToCsv(
            path.join(this._pathDir, `${room.id}.csv`),
            [
                {id: COL_ID, title: COL_ID},
                {id: COL_NAME, title: COL_NAME},
                {id: COL_CHRONO, title: COL_CHRONO},
                {id: COL_LIMITNBWORDS, title: COL_LIMITNBWORDS},
                {id: COL_TEAMPLAYINGORDER, title: COL_TEAMPLAYINGORDER},
            ],
            [{
                [COL_ID]:               room.id,
                [COL_NAME]:             room.name,
                [COL_CHRONO]:           room.chrono,
                [COL_LIMITNBWORDS]:     room.limitNbWords,
                [COL_TEAMPLAYINGORDER]: room.teamPlayingOrder,
            }]
        )
    }
}


module.exports = RoomFileAdapter