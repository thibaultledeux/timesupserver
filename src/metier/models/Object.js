class Object
{
  constructor(id, order = null)
  {
    this._id = id;
    this._order = order;
  }


  get id()
  {
    return this._id
  }


  get order()
  {
    return this._order;
  }
}


module.exports = Object