const Object = require('./Object')

class Step extends Object
{
  constructor(id = 0, name, done, order)
  {
    super(id, order)
    this._done = done
    this._name = name
  }

  toJSON() {
    return JSON.stringify({
      id:   this.id,
      done: this.done,
      name: this.name,
      order: this.order,
    })
  }

  get done()
  {
    return this._done
  }

  set done(value)
  {
    this._done = value
  }
  get name()
  {
    return this._name
  }

  set name(value)
  {
    this._name = value
  }

  get order()
  {
    return this._order
  }

  set order(value)
  {
    this._order = value
  }
}

module.exports = Step