const Object = require('./Object')

class Word extends Object
{
  constructor(id = 0, text, done)
  {
    super(id)
    this._text = text;
    this._done = done;
    this._id   = id;
  }

  toJSON () {
    return JSON.stringify({
      id:   this.id,
      text: this.text,
      done: this.done,
    })
  }

  get text()
  {
    return this._text;
  }

  set text(value)
  {
    this._text = value;
  }

  get done()
  {
    return this._done;
  }

  set done(value)
  {
    this._done = value;
  }
}

module.exports = Word