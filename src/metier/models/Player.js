const Object = require('./Object')

class Player extends Object
{
  constructor(id, name, ready, playing, order, isLastReady)
  {
    super(id, order)
    this._name        = name
    this._ready       = ready
    this._playing     = playing
    this._isLastReady = isLastReady
  }


  /**
   * convertie en json un joueur
   *
   * @return {string}
   */
  toJSON () {
    return JSON.stringify({
      id:          this.id,
      name:        this.name,
      ready:       this.ready,
      playing:     this.playing,
      isLastReady: this.isLastReady,
      order:       this.order,
    })
  }

  /**
   * restart le joueur
   */
  restart()
  {
    this.ready       = false;
    this.playing     = false;
    this.isLastReady = false;

  }

  get name()
  {
    return this._name
  }

  set name(value)
  {
    this._name = value
  }

  get ready()
  {
    return this._ready
  }

  set ready(value)
  {
    this._ready = value
  }

  get playing()
  {
    return this._playing
  }

  set playing(value)
  {
    this._playing = value
  }

  get order()
  {
    return this._order
  }

  set order(value)
  {
    this._order = value
  }

  get isLastReady()
  {
    return this._isLastReady
  }

  set isLastReady(value)
  {
    this._isLastReady = value
  }
}


module.exports = Player