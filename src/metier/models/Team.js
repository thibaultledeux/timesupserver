const Object = require('./Object')

class Team extends Object
{
  constructor(id = 0, players, score, order, playerPlayingOrder)
  {
    super(id, order)
    this._score              = score
    this._players            = players
    this._playerPlayingOrder = playerPlayingOrder
  }

  /**
   * set le prochain joueur de la team qui joue
   */
  setNextPlayerPlaying()
  {
    if (this.playerPlayingOrder === this.players.length - 1)
    {
      this.playerPlayingOrder = 0
    }
    else
    {
      this.playerPlayingOrder++
    }
    this.players.forEach((player) => {
      player.playing = (player.order === this.playerPlayingOrder)
    })
  }

  /**
   * set tous les joeurs en playing false
   */
  setPlayersNotPlaying()
  {
    this.players.forEach((player) => {
      player.playing = false
    })
  }

  /**
   * restart la team
   */
  restart()
  {
    this.score              = 0
    this.playerPlayingOrder = 0
    this.players.forEach((player) => {
      player.restart()
    })

  }

  /**
   * convertie la team en JSON
   *
   * @return {string}
   */
  toJSON()
  {
    let teamJSON = {
      id: this.id,
      score: this.score,
      order: this.order,
      playerPlayingOrder: this.playerPlayingOrder,
      players: [],
    }

    this.players.forEach((player) => {
      teamJSON.players.push(player.toJSON())
    })

    return JSON.stringify(teamJSON)
  }

  get players()
  {
    return this._players
  }


  set players(value)
  {
    this._players = value
  }


  get score()
  {
    return this._score
  }

  set score(value)
  {
    this._score = value
  }


  get order()
  {
    return this._order
  }

  set order(value)
  {
    this._order = value
  }


  get playerPlayingOrder()
  {
    return this._playerPlayingOrder
  }

  set playerPlayingOrder(value)
  {
    this._playerPlayingOrder = value
  }
}


module.exports = Team