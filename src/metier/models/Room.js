const Object = require('./Object')

class Room extends Object
{
  constructor(id = 0, name, chrono, limitNbWords, teamPlayingOrder, teams, words, steps)
  {
    super(id)
    this._name             = name
    this._teams            = teams
    this._words            = words
    this._steps            = steps
    this._chrono           = chrono
    this._limitNbWords     = limitNbWords
    this._teamPlayingOrder = teamPlayingOrder
  }

  /**
   * set le joueur qui joue
   *
   * @param {Team}  teamPlaying  la team qui joue
   */
  setPlayerPlaying(teamPlaying)
  {
    this.teams.forEach((team) => {
      if (team.id === teamPlaying.id)
      {
        team.setNextPlayerPlaying()
      }
      else
      {
        team.setPlayersNotPlaying()
      }
    })

  }

  /**
   * méloange les mots
   *
   * @returns {Word[]}
   */
  shuffleWords()
  {
    this._words.sort(() => Math.random() - 0.5)
  }

  /**
   * check si les mots existent deja dans la room
   * si oui retourne les doublons
   *
   * @returns {Word[]}
   */
  wordsExist(wordsToValidate)
  {
    let existWords = []
    this.words.forEach((word) => {
      wordsToValidate.forEach((wordToValidate) => {
        if (word.text.toUpperCase() === wordToValidate.text.toUpperCase())
        {
          if (existWords.filter(
                (existWord) => existWord.text.toUpperCase() === wordToValidate.text.toUpperCase()
             ).length === 0
          )
          {
            existWords.push(wordToValidate)
          }
        }
      })
    })

    return existWords
  }


  /**
   * check si le joueur est le dernier ready
   *
   * @param {Player}  player  un joueur
   *
   * @return {boolean}
   */
  isLastReady(player)
  {
    let nbPlayerNotReady = 0
    this.teams.forEach((team) => {
      nbPlayerNotReady += team.players.filter((player) => player.ready === false).length
    })

    return nbPlayerNotReady === 1
  }

  /**
   * set le joueur ready
   *
   * @param {Player}  playerReady  joueur ready
   */
  setPlayerReady(playerReady)
  {
    let isLastReady = this.isLastReady(playerReady)
    this.teams.forEach((team) => {
      team.players.forEach((player) => {
        if (player.id === playerReady.id)
        {
          player.ready        = true
          player.isLastReady = isLastReady
        }
        else if (player.ready === true)
        {
          player.isLastReady = false
        }
      })
    })
  }

  /**
   *
   * recupère la team suivant son ordre
   *
   * @param {number}  order  ordre
   *
   * @return {Team}
   */
  getTeamByOrder(order)
  {
    let teamOrder = null
    this.teams.forEach((team) => {
      if (team.order === order)
      {
        teamOrder = team
      }
    })

    if (teamOrder === null)
    {
      throw new Error(`Aucune nteam ne correspond a l'ordre ${order}`)
    }

    return teamOrder
  }

  /**
   * convertie en JSON la room
   *
   * @return {string}
   */
  toJSON()
  {
    let roomJson = {
      id: this.id,
      name: this.name,
      chrono: this.chrono,
      limitNbWords: this.limitNbWords,
      teamPlayingOrder: this.teamPlayingOrder,
      teams: [],
      words: [],
      steps: [],
    }

    this.teams.forEach((team) => {
      roomJson.teams.push(team.toJSON())
    })
    this.words.forEach((word) => {
      roomJson.words.push(word.toJSON())
    })
    this.steps.forEach((step) => {
      roomJson.steps.push(step.toJSON())
    })

    return JSON.stringify(roomJson)
  }

  /**
   * remet a zero les mots
   */
  restartWords()
  {
    this.words.forEach((word) => {
      word.done = false
    })
  }

  /**
   * remet a zero les steps
   */
  restartSteps()
  {
    this.steps.forEach((step) => {
      step.done = false
    })
  }

  /**
   * supprime tous les mots
   */
  cleanWords()
  {
    this.words = []
  }

  /**
   * restart les team de la room
   */
  restartTeams()
  {
    this.teams.forEach((team) => {
      team.restart()
    })
  }

  /**
   * restart une room
   */
  restart()
  {
    this.cleanWords()
    this.restartSteps()
    this.restartTeams()
    this.teamPlayingOrder = 0
  }

  get chrono()
  {
    return this._chrono
  }

  set chrono(value)
  {
    this._chrono = value
  }

  get teams()
  {
    return this._teams
  }

  get words()
  {
    return this._words
  }

  set words(value)
  {
    this._words = value
  }


  get limitNbWords()
  {
    return this._limitNbWords
  }

  set limitNbWords(value)
  {
    this._limitNbWords = value
  }

  get steps()
  {
    return this._steps
  }

  set steps(value)
  {
    this._steps = value
  }


  get teamPlayingOrder()
  {
    return this._teamPlayingOrder;
  }


  set teamPlayingOrder(value)
  {
    this._teamPlayingOrder = value;
  }


  get name()
  {
    return this._name;
  }
}

module.exports = Room