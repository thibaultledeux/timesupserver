const Team                = require('../metier/models/Team')
const DataFormatterPlayer = require('./DataFormatterPlayer')

class DataFormatterTeam
{
    constructor()
    {
        this._dataFormatterPlayer = new DataFormatterPlayer()
    }

    format(item)
    {
        let players = []
        if (true === item.hasOwnProperty('players'))
        {
            item.players.forEach((player) => {
                player = JSON.parse(player)
                players.push(this._dataFormatterPlayer.format(player))
            })
        }

        return new Team(item.id, players, item.score, item.order, item.playerPlayingOrder)
    }

}

module.exports = DataFormatterTeam