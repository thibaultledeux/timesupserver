const DataFormatterWord   = require('./DataFormatterWord')
const DataFormatterStep   = require('./DataFormatterStep')
const DataFormatterTeam   = require('./DataFormatterTeam')
const Room                = require('../metier/models/Room')

class DataFormatterRoom
{
    constructor()
    {
        this._dataFormatterWord   = new DataFormatterWord()
        this._dataFormatterStep   = new DataFormatterStep()
        this._dataFormatterTeam   = new DataFormatterTeam()
    }

    format(item)
    {
        let teams = []
        if (true === item.hasOwnProperty('teams'))
        {
            item.teams.forEach((team) => {
                team = JSON.parse(team)
                teams.push(this._dataFormatterTeam.format(team))
            })
        }

        let steps = []
        if (true === item.hasOwnProperty('steps'))
        {
            item.steps.forEach((step) => {
                step = JSON.parse(step)
                steps.push(this._dataFormatterStep.format(step))
            })
        }

        let words = []
        if (true === item.hasOwnProperty('words'))
        {
            item.words.forEach((word) => {
                word = JSON.parse(word)
                words.push(this._dataFormatterWord.format(word))
            })
        }

        return new Room(item.id, item.name, item.chrono, item.limitNbWords, item.teamPlayingOrder, teams, words, steps)
    }
}

module.exports = DataFormatterRoom