const Step = require('../metier/models/Step')

class DataFormatterStep
{
    format(item)
    {
        return new Step(item.id, item.name, item.done, item.order)
    }
}

module.exports = DataFormatterStep