const Word = require('../metier/models/Word')

class DataFormatterWord
{
    format(item)
    {
        return new Word(item.id, item.text, item.done)
    }

}

module.exports = DataFormatterWord