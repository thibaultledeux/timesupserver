const Player = require('../metier/models/Player')

class DataFormatterPlayer
{
    format(item)
    {
        return new Player(item.id, item.name, item.ready, item.playing, item.order, item.isLastReady)
    }
}

module.exports = DataFormatterPlayer