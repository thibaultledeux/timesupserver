const DataFormatterWord   = require('./DataFormatterWord')
const DataFormatterStep   = require('./DataFormatterStep')
const DataFormatterTeam   = require('./DataFormatterTeam')
const DataFormatterRoom   = require('./DataFormatterRoom')
const DataFormatterPlayer = require('./DataFormatterPlayer')

class DataFormatterRouter
{
    /**
     * constructor
     *
     * @param {array}  dataToFormat  tableau de données a formater
     */
    constructor(dataToFormat= [])
    {
        this._dataToFormat        = dataToFormat
        this._dataFormatterWord   = new DataFormatterWord()
        this._dataFormatterStep   = new DataFormatterStep()
        this._dataFormatterTeam   = new DataFormatterTeam()
        this._dataFormatterRoom   = new DataFormatterRoom()
        this._dataFormatterPlayer = new DataFormatterPlayer()
    }

    format()
    {
        if (this._dataToFormat === null)
        {
            return null
        }

        let data = []
        this._dataToFormat.forEach((item) => {
            item = JSON.parse(item)
            switch (item.formatter)
            {
                case 'room':
                    data.push(this._dataFormatterRoom.format(item))
                    break
                case 'word':
                    data.push(this._dataFormatterWord.format(item))
                    break
                case 'step':
                    data.push(this._dataFormatterStep.format(item))
                    break
                case 'team':
                    data.push(this._dataFormatterTeam.format(item))
                    break
                case 'player':
                    data.push(this._dataFormatterPlayer.format(item))
                    break
                default:
                    throw new Error(`${__filename} : Formatter inconnu ${item.formatter}`)
            }
        })

        return data
    }
}


module.exports = DataFormatterRouter