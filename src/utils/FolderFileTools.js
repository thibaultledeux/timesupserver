const fs   = require('fs');
const path = require('path');

class FolderFileTools
{
    /**
     * crer le dossier
     *
     * @param {string}  dir  path du sissier
     *
     * @returns {boolean}
     */
    static createFolder(dir)
    {
        try {
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir)
                return true
            }
        } catch (e) {
            throw e
        }
    }

    /**
     * verifie si le fichier existe
     *
     * @param {string}  pathFile  chemin du fichier
     *
     * @returns {boolean}
     */
    static existFile(pathFile)
    {
        try {
            return fs.existsSync(pathFile)
        } catch(e) {
            throw e
        }
    }

    /**
     * determine cest un fichier
     *
     * @param {string}  pathFile  chemin du fichier
     *
     * @returns {boolean}
     */
    static isFile (pathFile) {
        try
        {
            return fs.lstatSync(pathFile).isFile()
        }
        catch (e)
        {
            throw e
        }
    }

    /**
     * determine cest un doccier
     *
     * @param {string}  pathDir  chemin du dossier
     *
     * @returns {boolean}
     */
    static isDir (pathDir) {
        try
        {
            return fs.lstatSync(pathDir).isDirectory()
        }
        catch (e)
        {
            throw e
        }
    }

    /**
     * supprime le dossier et tous ses enfant
     *
     * @param {string}  pathToDelete  chemin du dossier
     */
    static deleteFolderAndChildren(pathToDelete)
    {
        if (false === FolderFileTools.isDir(pathToDelete))
        {
            throw new Error(`aucun dossier existant ${pathToDelete}`)
        }

        const files = fs.readdirSync(pathToDelete)
        if (files.length > 0)
        {
            files.forEach((filename) => {
                let pathDir = path.join(pathToDelete, filename)
                if (FolderFileTools.isDir(pathDir))
                {
                    FolderFileTools.deleteFolderAndChildren(pathDir)
                }
                else
                {
                    fs.unlinkSync(pathDir)
                }
            })
            fs.rmdirSync(pathToDelete)
        }
        else
        {
            fs.rmdirSync(pathToDelete)
        }
    }
}

module.exports = FolderFileTools