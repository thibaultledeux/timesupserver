var Readable = require('stream').Readable;
var Writable = require('stream').Writable;
var util = require('util');

class Watcher
{
    constructor(propertyWatch, subscriber, unsubscribe)
    {
        this._propertyWatch = propertyWatch;
        this._subscriber    = subscriber;
        this._unsubscribe   = unsubscribe;
    }
}