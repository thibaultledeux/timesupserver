'use strict';
const moment       = require('moment');
const fs           = require('fs');
const ERROR        = 'ERROR : ';
const INFO         = 'INFO : ';
const pathLogFile  = './log/log.txt';
class Logger
{
    static error(message, roomId)
    {
        let txt = ERROR + moment().format('Y-MM-DD HH:mm:ss') + ' : ' + roomId + ' : ' + message + '\n';
        console.trace(txt);
        fs.appendFile(pathLogFile, txt, (error) =>
        {
            if (error) throw error;
        });
    }

    static info(message, roomId)
    {
        let txt = INFO + moment().format('Y-MM-DD HH:mm:ss') + ' : ' + roomId + ' : ' + message + '\n';
        console.log(txt);
        fs.appendFile(pathLogFile, txt, (error) =>
        {
            if (error) throw error;
        });
    }
}

module.exports = Logger;