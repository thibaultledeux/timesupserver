class SocketResponse
{
    constructor(
        routeOrigin,
        roomId,
        data = null,
        action = null,
        success = true,
        broadcast = true,
        redirect = null
    )
    {
        this._routeOrigin = routeOrigin
        this._data      = data
        this._action    = action
        this._roomId    = roomId
        this._success   = success
        this._redirect  = redirect
        this._broadcast = broadcast
    }


    get broadcast()
    {
        return this._broadcast;
    }


    get redirect()
    {
        return this._redirect;
    }

    get roomId()
    {
        return this._roomId;
    }
}

module.exports = SocketResponse