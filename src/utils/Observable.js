var Readable = require('stream').Readable;
var Writable = require('stream').Writable;
var util = require('util');

module.exports = {
    Observable : (subscriber) => {
        this.subscribe    = subscriber
        this.fromReadable = (readable) => {
            return new Observable(function(observer) {
                function nop() {}

                var nextFn = observer.next ? observer.next.bind(observer) : nop
                var returnFn = observer.return ? observer.return.bind(observer) : nop
                var throwFn = observer.throw ? observer.throw.bind(observer) : nop

                readable.on('data', nextFn)
                readable.on('end', returnFn)
                readable.on('error', throwFn)

                return new Subscription(function() {
                    readable.removeListener('data', nextFn)
                    readable.removeListener('end', returnFn)
                    readable.removeListener('error', throwFn)
                })
            })
        }
    },
    Subscription : (unsubscribe) => {
        this.unsubscribe = unsubscribe
    },
    Observer: function(handlers) {
        function nop() {}

        this.next = handlers.next || nop
        this.return = handlers.return || nop
        this.throw = handlers.throw || nop
        this.fromWritable = function(writable, shouldEnd, throwFn) {
            return new Observer({
                next: writable.write.bind(writable),
                return: shouldEnd ? writable.end.bind(writable) : function() {},
                throw: throwFn
            });
        }
    }
}