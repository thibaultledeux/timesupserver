const SocketRouter = require('./SocketRouter')

class SocketQueueRoute
{
    constructor()
    {
        this._queueRoute          = []
        this._canExecuteNextRoute = true
    }

    /**
     * ajoute une route
     *
     * @param {io}      io        systeme io
     * @param {socket}  socket    le socket de la route
     * @param {string}  data      les données envoyés
     */
    async addRoute(io, socket, data)
    {
        // parse les données recues
        data = JSON.parse(data)

        // rejoins la room au socket
        if (data.roomId !== 0)
        {
            socket.join(data.roomId)
        }

        // ajoute a la queue
        this._queueRoute.push(new SocketRouter(io, socket, data.roomId, data.route, data.data))

        // si aucune route est en cours dexecution, on lance l'exécution
        if (this._canExecuteNextRoute === true)
        {
            await this.executeNextRoute()
        }
    }

    /**
     * exceute la route suivante
     * @return {Promise<void>}
     */
    async executeNextRoute()
    {
        if (this._queueRoute.length > 0)
        {
            this._canExecuteNextRoute    = false
            const socketRouterToExcecute = this._queueRoute[0]
            this._canExecuteNextRoute    = await socketRouterToExcecute.executeRoute()
            this._queueRoute.shift()

            if (this._queueRoute.length > 0)
            {
                await this.executeNextRoute()
            }
        }

    }
}

module.exports = SocketQueueRoute