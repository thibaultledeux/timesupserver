const Logger              = require('../utils/Logger')
const SocketResponse      = require('../utils/SocketResponse')
const SocketController    = require('../controllers/SocketController')
const DataFormatterRouter = require('../dataFormatters/DataFormatterRouter')

class SocketRouter
{
    constructor(io, socket, roomId, route, data)
    {
        this._io     = io
        this._data   = data
        this._route  = route
        this._roomId = roomId
        this._socket = socket
    }

    /**
     * format les donnes en classes metiers
     *
     * @return {boolean}
     */
    formatData()
    {
        try
        {
            const dataFormatterRouter = new DataFormatterRouter(this._data)
            this._data                = dataFormatterRouter.format()

            return true
        }
        catch (e)
        {
            throw e
        }

    }

    /**
     * envoie la response
     *
     * @param {SocketResponse}  response  la response
     */
    sendResponse(response)
    {
        // envoi de la reponse
        if (response.broadcast === true)
        {
            console.log('send response bradcast')
            this._io.sockets.in(response.roomId).emit('response', response)
        }
        else
        {
            console.log('send response simple')
            // retourne la reponse
            this._socket.emit('response', response)
        }

        return true
    }

    /**
     * bordcast sur tous les socket de la room pour start le chrono
     */
    sendRequestToStartChrono()
    {
        console.log('sendRequestToStartChrono send')
        this._io.sockets.in(this._roomId).emit('startStep')
    }

    /**
     * execute une route
     *
     * @return {Promise<boolean>}
     */
    async executeRoute()
    {

        console.log('route executé ', this._route)
        if (this._route === 'startChrono')
        {
            this.sendRequestToStartChrono()
        }
        else
        {
            let response  = null
            try
            {
                this.formatData()
                let data               = null
                const socketController = new SocketController(this._roomId, this._data)
                switch (this._route)
                {
                    case 'getRooms':
                        data     = await socketController.getRooms()
                        response = new SocketResponse(this._route, this._roomId, data, `setRooms`, true, false)
                        break

                    case 'getRoom':
                        data     = await socketController.getRoom()
                        response = new SocketResponse(this._route, this._roomId, data, `setRoomSelected`, true, false)
                        break

                    case 'setRooms':
                        data     = await socketController.setRooms()
                        response = new SocketResponse(this._route, this._roomId)
                        console.log('route setRooms success')
                        break

                    case 'createWords':
                        data     = await socketController.createWords()
                        response = new SocketResponse(this._route, this._roomId, data, `setRoomSelected`)
                        break

                    case 'setPlayerReady':
                        data     = await socketController.setPlayerReady()
                        response = new SocketResponse(this._route, this._roomId, data, `setRoomSelected`)
                        break

                    case 'saveTurn':
                        data     = await socketController.saveTurn()
                        response = new SocketResponse(this._route, this._roomId, data, `setRoomSelected`)
                        break

                    case 'setPlayerPlaying':
                        data     = await socketController.setPlayerPlaying()
                        response = new SocketResponse(this._route, this._roomId, data, `setRoomSelected`)
                        console.log('route setPlayerPlaying success')
                        break

                    case 'restartRoom':
                        data     = await socketController.restartRoom()
                        response = new SocketResponse(this._route, this._roomId, data, `setRoomSelected`, true, true, 'createWords')
                        break

                    case 'deleteRoom':
                        await socketController.deleteRoom()
                        response = new SocketResponse(this._route, this._roomId, null, null, true, true, 'home')
                        break

                    case 'validateWords':
                        data = await socketController.validateWords()
                        response = new SocketResponse(this._route, this._roomId, data, 'setExistWords', true, false)
                        break

                    default:
                        throw new Error(`${__filename} : route inconnue roomId : ${this._roomId} / route : ${this._route}`)
                }

                return this.sendResponse(response)
            }
            catch (e)
            {
                Logger.error(e, this._roomId)
                response = new SocketResponse(this._route, this._roomId, null, this._route, false)
                return this.sendResponse(response)
            }
        }
    }

    get roomId ()
    {
        return this._roomId
    }
}

module.exports = SocketRouter
