const http             = require('http')
const express          = require('express')
const Logger           = require('./src/utils/Logger.js')
const SocketQueueRoute = require('./src/router/SocketQueueRoute')

// init l'app
const app = express()
app.get('/', (req, res) => res.send('Hello World!'))
//initialize a simple http server
const server = http.createServer(app)

// Chargement de socket.io
const io = require('socket.io').listen(server)

// init le systeme de queue des socket
const socketQueueRoute = new SocketQueueRoute()

// Quand un client se connecte, on le note dans la console
io.on('connection', function (socket) {

    socket.on('disconnect', () => {
        console.log('socket disconnected')
    })

    console.log('Un client est connecté !')
    socket.on('request', async (data) => {
        // init les paramètres
        await socketQueueRoute.addRoute(io, socket, data)
    })
})

//start our server
server.listen(process.env.PORT, () => {
    Logger.info(`Server started on port ${server.address().port} :)`)
})
